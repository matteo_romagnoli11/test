  package application;

import javafx.application.Application;
import javafx.stage.Stage;
import view.AppView;

public final class Main extends Application {

    @Override
    public void start (final Stage stage) throws Exception {
        new AppView(stage);
    }

    public static void main(final String[] args) {
        launch();
    }

}

